package pl.kaclucz.proj;

import javax.swing.*;
import java.awt.*;

public class MainCanvas extends JPanel { //VIEW (MVC)
    private final int fieldSize;
    private final int size;
    public MainCanvas(int fieldSize, int size) {
        setSize(801, 801);
        this.fieldSize = fieldSize;
        this.size = size;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.GREEN);
        g.fillRect(0,0,800,800);
        g.setColor(Color.BLACK);
        paintBoard(g);
    }

    public void paintOrg(int x, int y, String id) {
        Graphics g = this.getGraphics();
        g.setColor(idToColor(id));
        g.fillRect(x*fieldSize+1, y*fieldSize+1, fieldSize-1, fieldSize-1);
    }

    public void reset() {
        Graphics g = this.getGraphics();
        g.setColor(Color.green);
        g.fillRect(0,0,800,800);
        g.setColor(Color.BLACK);
        paintBoard(g);
    }

    private void paintBoard(Graphics g) {
        for (int x = 0; x <= size ; x++)
        {
            g.drawLine(fieldSize*x, 0, fieldSize*x, 800);
        }
        for (int y = 0; y <= size ; y++)
        {
            g.drawLine(0,fieldSize*y, 800, fieldSize*y);
        }
    }

    private Color idToColor(String id) {
        if (id.equals("Wolf")) return Color.DARK_GRAY;
        else if(id.equals("Sheep")) return Color.WHITE;
        else if (id.equals("Antelope")) return new Color(180, 127, 39);
        else if (id.equals("Mouse")) return new Color(173, 170, 170);
        else if (id.equals("Elephant")) return new Color(116, 91, 84);
        else if(id.equals("Grass")) return new Color(12, 95, 18);
        else if(id.equals("Guarana")) return Color.RED;
        else if(id.equals("Belladona")) return new Color(101, 56, 142);
        else return Color.GREEN;
    }
}
