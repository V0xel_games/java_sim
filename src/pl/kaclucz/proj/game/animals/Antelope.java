package pl.kaclucz.proj.game.animals;

import pl.kaclucz.proj.game.Organism;
import pl.kaclucz.proj.game.World;
import pl.kaclucz.proj.game.exceptions.NoFreePositionException;
import pl.kaclucz.proj.game.util.Logger;
import pl.kaclucz.proj.game.util.RandomHelper;
import java.awt.Point;
import java.util.Optional;

public class Antelope extends Animal {
    private static final double ESCAPE_PROB = 0.5;

    public Antelope(int x, int y, World world) {
        super(x, y, world, "Antelope", 4, 4, 2);
    }

    private Optional<Point> findFreePos() { //Optional-- wrapper na cos co moze byc null - mozna by przerobic ze
        Point p = null;                                   //closeFreePos Optional ale wtedy bez wyjatku by bylo
        try {
            p = super.closeFreePos();
        } catch (NoFreePositionException ignored) {
        }
        return Optional.ofNullable(p);
    }

    @Override
    public void collision(boolean isAttacking, Organism other) {
        Optional<Point> newPosition = findFreePos();
        if(RandomHelper.randomBoolean(ESCAPE_PROB) && newPosition.isPresent()) { //is present - rozne od null
            Point currPosition = position;

            moveIn2D(newPosition.get());
            Logger.getInstance().addEscapeEvent(id);
            if(!isAttacking) {
                other.moveIn2D(currPosition);
            }
        } else {
            if(isAttacking) {
                other.collision(false, this);
            } else {
                super.collision(false, other);
            }
        }
    }
}
