package pl.kaclucz.proj.game.animals;
import pl.kaclucz.proj.game.World;

public class Wolf extends Animal
{
    public Wolf(int x, int y, World world) {
        super(x, y, world, "Wolf", 9, 5, 1);
    }

}
