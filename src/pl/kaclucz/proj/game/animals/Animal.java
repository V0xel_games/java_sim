package pl.kaclucz.proj.game.animals;
import pl.kaclucz.proj.game.Organism;
import pl.kaclucz.proj.game.World;
import pl.kaclucz.proj.game.util.Logger;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public abstract class Animal extends Organism
{
    private int movementSize;

    protected Animal(int x, int y, World world, String id, int strength, int initative, int movementSize) {
        super(x, y, world, id, strength, initative);
        this.movementSize=movementSize;
    }

    @Override
    public void action() {
        Point newPosition = findNewPos();
        if (world.isEmpty(newPosition)) {
            moveIn2D(newPosition);
            return;
        }

        Organism otherOrg = world.getByPos(newPosition);

        if (id.equals(otherOrg.getId())){
            multiplicate();
            return;
        }
        collision(true,otherOrg);
    }

    @Override
    public void collision(boolean isAttacking, Organism other) {
        if (isAttacking){
            other.collision(false,this);
        }
        else {
            if (strength > other.getStrength()) {
                other.setToDeletion(true);
                other.moveIn2D(other.getPosition());
                Logger.getInstance().addDefenseWinEvent(id, other.getId());
            }
            else {
                setToDeletion(true);
                other.moveIn2D(position);
                Logger.getInstance().addAttackWinEvent(other.getId(), id);
            }
        }
    }

    private Point findNewPos() {
        List<Point> buffor = new ArrayList<>();
        int right = position.x + movementSize;
        int left = position.x - movementSize;
        int down = position.y + movementSize;
        int up = position.y - movementSize;

        if (right<=world.getMAX_POS()) {
            buffor.add(new Point(right, position.y));
        }
        if (left>=world.getMIN_POS()) {
            buffor.add(new Point(left, position.y));
        }
        if (down<=world.getMAX_POS()) {
            buffor.add(new Point(position.x, down));
        }
        if (up>=world.getMIN_POS()) {
            buffor.add(new Point(position.x, up));
        }
        int index = ThreadLocalRandom.current().nextInt(0, buffor.size()); //bez -1 bo bound jest "otwarty"
        return buffor.get(index);
    }
}
