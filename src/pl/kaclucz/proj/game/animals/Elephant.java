package pl.kaclucz.proj.game.animals;

import pl.kaclucz.proj.game.World;
import pl.kaclucz.proj.game.util.RandomHelper;

public class Elephant extends Animal {
    private final static double MOVE_PROBABILITY = 0.6;
    public Elephant(int x, int y, World world) {
        super(x, y, world, "Elephant", 9, 3, 1);
    }

    @Override
    public void action() {
        super.action();
        if (!isToDeletion() && RandomHelper.randomBoolean(MOVE_PROBABILITY)){
            super.action();
        }
    }
}
