package pl.kaclucz.proj.game.animals;

import pl.kaclucz.proj.game.Organism;
import pl.kaclucz.proj.game.World;
import pl.kaclucz.proj.game.exceptions.NoFreePositionException;
import pl.kaclucz.proj.game.util.Logger;

import java.awt.Point;
import java.util.Objects;

public class Mouse extends Animal {

    public Mouse(int x, int y, World world) {
        super(x, y, world, "Mouse", 1, 6, 1);
    }

    @Override
    public void collision(boolean isAttacking, Organism other) {
        if (isAttacking){
            other.collision(false, this);
        } else {
            try {
                Point newPosition = closeFreePos();
                Point currPosition = position;
                moveIn2D(newPosition);
                if(!Objects.isNull(other)) {
                    other.moveIn2D(currPosition);
                    Logger.getInstance().addEscapeEvent(id);
                }
            } catch (NoFreePositionException ignored) {
                super.collision(false, other);
            }
        }
    }
}
