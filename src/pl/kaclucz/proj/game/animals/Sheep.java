package pl.kaclucz.proj.game.animals;

import pl.kaclucz.proj.game.World;

public class Sheep extends Animal {
    public Sheep(int x, int y, World world) {
        super(x, y, world, "Sheep", 4, 4, 1);
    }
}
