package pl.kaclucz.proj.game.util;
import pl.kaclucz.proj.game.World;

@FunctionalInterface
public interface CreateOrg<T>{
    T get(int x, int y, World world);
}
