package pl.kaclucz.proj.game.util;

public final class Logger //Singleton
{
    private static final Logger INSTANCE = new Logger();
    private StringBuilder logBuilder = new StringBuilder();

    private Logger()
    {
    }

    public static Logger getInstance()
    {
        return INSTANCE;
    }

    public void addAttackWinEvent(final String winner, final String looser) {
        logBuilder.append(winner).append(" kills ").append(looser).append("\n");
    }

    public void addDefenseWinEvent(final String winner, final String looser) {
        logBuilder.append(looser).append(" fails attack on ").append(winner).append("\n");
    }

    public void addEatEvent(final String animal, final String plant) {
        logBuilder.append(animal).append(" eats ").append(plant).append("\n");
    }

    public void addSpawnEvent(final String org) {
        logBuilder.append(org).append(" multiplies ").append("\n");
    }

    public void addEscapeEvent(final String org) {
        logBuilder.append(org).append(" escapes ").append("\n");
    }

    public String getLog() {
        return logBuilder.toString();
    }

    public void clean() {
        logBuilder = new StringBuilder();
    } //brak clear, najprostsze rozwiazanie, i tak mamy GC :D
}
