package pl.kaclucz.proj.game.util;

import java.util.Random;

public class RandomHelper {
    private static final Random RANDOM = new Random();

    public static boolean randomBoolean(double prob){
        return RANDOM.nextDouble() < prob;
    }
}
