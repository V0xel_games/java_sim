package pl.kaclucz.proj.game.util;
import pl.kaclucz.proj.game.Organism;
import pl.kaclucz.proj.game.World;
import java.util.HashMap;
import java.util.Map;

public final class Factory
{
    private Map<String, CreateOrg<? extends Organism>> map = new HashMap<>();

    public void register(String name, CreateOrg<? extends Organism> supplier) {
        map.put(name, supplier);
    }

    public Organism produce(String name, int x, int y, World world) {
        return map.get(name).get(x, y, world);
    }
}
