package pl.kaclucz.proj.game;

import pl.kaclucz.proj.game.exceptions.NoFreePositionException;
import pl.kaclucz.proj.game.util.Logger;

import java.awt.Point;


public abstract class Organism
{
    protected World world;
    protected Point position;
    protected String id;
    protected int strength;
    private int initative;
    private boolean toDeletion;

    protected Organism(int x, int y, World world, String id, int strength, int initative ) {
        this.world=world;
        this.position = new Point(x, y);
        this.toDeletion = false;
        this.id=id;
        this.strength=strength;
        this.initative=initative;
    }
    public abstract void action();
    public abstract void collision(boolean isAttacking, Organism other);

    public final boolean isToDeletion() { return toDeletion; }
    public final Point getPosition() { return position; }
    public final String getId() { return id; }
    public final int getInitative() { return initative; }
    public final int getStrength() { return strength; }
    public final void setStrength(int strength) { this.strength = strength; }
    public final void setToDeletion(boolean toDeletion) { this.toDeletion = toDeletion; }

    public final void moveIn2D(Point newPos){
        world.setNew2dPos(position, newPos);
        position=newPos;
    }

    protected final Point closeFreePos() throws NoFreePositionException {
        int right = position.x + 1;
        int left = position.x - 1;
        int down = position.y + 1;
        int up = position.y - 1;

        if (right <=world.getMAX_POS() && world.isEmpty(new Point(right,position.y))) {
            return new Point(right, position.y);
        }
        if (left >= world.getMIN_POS() && world.isEmpty(new Point(left, position.y))) {
            return new Point(left, position.y);
        }
        if (down <= world.getMAX_POS() && world.isEmpty(new Point(position.x, down))) {
            return new Point(position.x, down);
        }
        if (up >= world.getMIN_POS() && world.isEmpty(new Point(position.x, up))) {
            return new Point(position.x, up);
        }
        throw new NoFreePositionException();
    }

    protected final void multiplicate(){
        try {
            Point spawnPosition = closeFreePos();
            Logger.getInstance().addSpawnEvent(id);
            world.spawnOrganism(spawnPosition.x,spawnPosition.y, getId());
        } catch (NoFreePositionException ignored) {
        }
    }


}
