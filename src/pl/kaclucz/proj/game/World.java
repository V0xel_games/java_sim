package pl.kaclucz.proj.game;

import pl.kaclucz.proj.game.plants.Belladona;
import pl.kaclucz.proj.game.plants.Grass;
import pl.kaclucz.proj.game.plants.Guarana;
import pl.kaclucz.proj.game.animals.*;
import pl.kaclucz.proj.game.util.Factory;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public final class World {
    private static final int MAX_POS = 19;
    private static final int MIN_POS = 0;

    private Factory factory;
    private Organism[][] array2D;
    private List<Organism> sortedOrganisms;
    private List<Organism> toAddOrganisms;
    private boolean isFirstTurn;

    public World() {
        array2D=new Organism[20][20];
        factory = new Factory();
        sortedOrganisms = new ArrayList<>();
        toAddOrganisms = new ArrayList<>();
        isFirstTurn = true;
        registerOrganisms();
    }

    public void nextTurn() {
        if (isFirstTurn){
            firstTurn();
            cleanup();
            isFirstTurn=false;
            return;
        }
        for (Organism element: sortedOrganisms) {
            if (!element.isToDeletion()){
                element.action();
            }
        }
        cleanup();
    }

    public boolean isEmpty(Point pos) {
        return Objects.isNull(array2D[pos.x][pos.y]);
    }

    public Organism getByPos(Point pos) {
        return array2D[pos.x][pos.y];
    }

    public int getMAX_POS() {
        return MAX_POS;
    }

    public int getMIN_POS() {
        return MIN_POS;
    }

    public List<Organism> getSortedOrganisms() {
        return sortedOrganisms;
    }

    public void setNew2dPos(Point oldPos, Point newPos) {
        if(Objects.isNull(array2D[oldPos.x][oldPos.y])) {
            throw new IllegalStateException("Org is null");
        }
        array2D[newPos.x][newPos.y]=array2D[oldPos.x][oldPos.y];
        array2D[oldPos.x][oldPos.y]=null;
    }

    public void spawnOrganism(int x, int y, String id) {
        array2D[x][y]=factory.produce(id, x, y, this);
        toAddOrganisms.add(array2D[x][y]);
    }

    private void addOrganismInRandomPos(String id) {
        while (true){
            int x =  ThreadLocalRandom.current().nextInt(MIN_POS, MAX_POS+1);
            int y = ThreadLocalRandom.current().nextInt(MIN_POS, MAX_POS+1);
            if (isEmpty(new Point(x, y))){
                spawnOrganism(x,y,id);
                return;
            }
        }
    }

    private void cleanup() {
        sortedOrganisms.addAll(toAddOrganisms);
        toAddOrganisms.clear();
        sortedOrganisms.sort(Comparator.comparingInt(Organism::getInitative).reversed());
        sortedOrganisms = sortedOrganisms.stream()
                                .filter(((Predicate<Organism>)Organism::isToDeletion).negate())
                                .collect(Collectors.toList());
    }

    private void registerOrganisms() {
        factory.register("Wolf", Wolf::new);
        factory.register("Sheep", Sheep::new);
        factory.register("Antelope", Antelope::new);
        factory.register("Mouse", Mouse::new);
        factory.register("Elephant", Elephant::new);
        factory.register("Grass", Grass::new);
        factory.register("Guarana", Guarana::new);
        factory.register("Belladona", Belladona::new);
    }

    private void firstTurn() {
        for (int i = 0; i < 5 ; i++) {
            addOrganismInRandomPos("Wolf");
            addOrganismInRandomPos("Sheep");
            addOrganismInRandomPos("Antelope");
            addOrganismInRandomPos("Mouse");
            addOrganismInRandomPos("Elephant");
            addOrganismInRandomPos("Grass");
            addOrganismInRandomPos("Guarana");
            addOrganismInRandomPos("Belladona");
        }
    }
}
