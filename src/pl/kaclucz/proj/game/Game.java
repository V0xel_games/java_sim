package pl.kaclucz.proj.game;

import java.util.List;

public final class Game
{
    public List<Organism> getToPaint(){
        return world.getSortedOrganisms();
    }
    private World world = new World();

    public void startGame(){
        world.nextTurn();
    }
}
