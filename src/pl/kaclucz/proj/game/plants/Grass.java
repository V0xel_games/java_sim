package pl.kaclucz.proj.game.plants;

import pl.kaclucz.proj.game.World;

public class Grass extends Plant {
    public Grass(int x, int y, World world) {
        super(x, y, world, "Grass");
    }
}
