package pl.kaclucz.proj.game.plants;

import pl.kaclucz.proj.game.Organism;
import pl.kaclucz.proj.game.World;
import pl.kaclucz.proj.game.util.Logger;
import pl.kaclucz.proj.game.util.RandomHelper;

import java.util.Random;


public abstract class Plant extends Organism {
    private final static double SPAWN_PROBABILITY = 0.17;
    private Random random = new Random();

    protected Plant(int x, int y, World world, String id) {
        super(x, y, world, id, 0, 0);
    }

    @Override
    public void action() {
        if (getSpawnChance()){
            multiplicate();
        }
    }

    @Override
    public void collision(boolean isAttacking, Organism other) {
        setToDeletion(true);
        other.moveIn2D(position);
        Logger.getInstance().addEatEvent(other.getId(), id);
    }

    private boolean getSpawnChance(){
        return RandomHelper.randomBoolean(SPAWN_PROBABILITY);
    }
}
