package pl.kaclucz.proj.game.plants;

import pl.kaclucz.proj.game.Organism;
import pl.kaclucz.proj.game.World;


public class Guarana extends Plant {

    private final int strengthBoost = 3;

    public Guarana(int x, int y, World world) {
        super(x, y, world, "Guarana");
    }

    @Override
    public void collision(boolean isAttacking, Organism other) {
        other.setStrength(other.getStrength()+strengthBoost);
        super.collision(isAttacking, other);
    }
}
