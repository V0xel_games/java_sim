package pl.kaclucz.proj.game.plants;

import pl.kaclucz.proj.game.Organism;
import pl.kaclucz.proj.game.World;
import pl.kaclucz.proj.game.util.Logger;

public class Belladona extends Plant {
    public Belladona(int x, int y, World world) {
        super(x, y, world, "Belladona");
    }

    @Override
    public void collision(boolean isAttacking, Organism other) {
        setToDeletion(true);
        moveIn2D(position);
        other.setToDeletion(true);
        other.moveIn2D(other.getPosition());
        Logger.getInstance().addEatEvent(other.getId(), id);
    }
}
