package pl.kaclucz.proj;

import pl.kaclucz.proj.game.Game;
import pl.kaclucz.proj.game.Organism;
import pl.kaclucz.proj.game.util.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainPanel extends JPanel implements ActionListener { //This is actual controller (MVC)
    private JButton nextTurnButton;
    private JTextArea text;
    private JScrollPane scroller;
    private MainCanvas canvas;
    private Game game;
    private final int size = 20;

    public MainPanel() {
        setLayout(null);
        setSize(1015, 870);
        nextTurnButton = new JButton("Next turn");

        text = new JTextArea();
        text.setEditable(false);
        text.setBackground(Color.LIGHT_GRAY);
        text.setWrapStyleWord(true);

        nextTurnButton.setBounds(0, 801, 800, 40 );
        nextTurnButton.addActionListener(this );
        game = new Game();
        canvas = new MainCanvas(40, size);

        scroller = new JScrollPane(text);
        scroller.setBounds(802, 0, 210, 840);
        scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        add(scroller);
        add(nextTurnButton);
        add(canvas);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == nextTurnButton) {
            game.startGame();
            canvas.reset();
            text.setText(null);
            for (Organism element:game.getToPaint()) {
                canvas.paintOrg(element.getPosition().x, element.getPosition().y, element.getId());
            }
            text.append(Logger.getInstance().getLog());
            Logger.getInstance().clean();
        }
    }
}
