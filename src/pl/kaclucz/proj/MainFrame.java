package pl.kaclucz.proj;

import javax.swing.*;

public class MainFrame extends JFrame {

    public MainFrame() {
        super("Kacper Łuczak, 167947");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLayout(null);
        setSize(1017, 871);
        setLocation(150, 30);
        JPanel panel = new MainPanel();
        add(panel);
        setVisible(true);
        createBufferStrategy(3); //It helps with flickering
    }
}
